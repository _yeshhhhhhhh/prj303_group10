# DocBot Disease Recognition



## Gyalpozhing College of Information Technology(Bachelor of Science in Information Technology) Third Year Project

DocBot is a web application create by BScIT (Group 10) as its third year project. This web application was built to help any individual to know what they are suffering from with the provided symptoms. DocBot uses the features provided by DialogFlow and enable the user to chat with bot just like any other human being. The Bot will take into consideration all the provided symptoms and identify the disease with a brief description.

Check out the web application <a href="http://docbot-pocketdoc.herokuapp.com/" target = "_blank">here!</a>

To use this application, you don't have to give any user credential. You can instantly start chatting with the Bot.

Built using:HTML/CSS/JS, DialogFlow

<i>This application does not include any sensitive information of anyone.</i>

## Images and Videos
Showcase video: https://www.youtube.com/watch?v=2gsDQZY1CRs

Screenshot of the application:
<img src="https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/Home.png"></img>

![About1](https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/About1.png)

![About2](https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/About2.png)

![Review](https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/Review.png)

![News](https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/News1.png)

![Team](https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/Team.png)

![Footer](https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/Footer.png)

<img src="https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/Bot2.jpg" width="400" height="400"></img>

<img src="https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/Bot3.jpg" width="400" height="400"></img>



## Final Poster
<img src="https://gitlab.com/_yeshhhhhhhh/prj303_group10/-/raw/main/Screenshots_Poster/poster_final.png" width="400" height="700"></img>

## Releases

No releases published

## Packages

No packages published
